# AI-VISOR-METRICS

This repo provides a streamlit dashboard that interacts with a MongoDB cluster to create simple visualizations each clients data. 

## Installation 

Using **pipenv** (you can create a pipfile from the provided requirements.txt):
	
	pipenv install


Or if you already a dedicated environment where you want install packages you can do:

	pip install requirements.txt
	

Finally if you an **Anaconda python distribuition** you can use the enviroment.yml file provided:

	conda env create --file environment.yml
	
## Running

To run the project locally we will use the **.env file** available inside the config folder. 

In the the root folder type 

	streamlit run app/main.py 

and an url will be launched in your browser with the dashboard

- NOTE: do not forget to activate your vpn in the correct location and also ssh cmd to the db. Project expects development db routing in the 27019 port bit you can configure that inside the .env file  

	
