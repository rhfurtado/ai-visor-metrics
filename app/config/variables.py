"""

config/variables.py

Contains all the variables of the API.

"""

# Import

## Python packages
import os
import ast
from dotenv import load_dotenv

## App files
def load():         
    # Server config
    global PORT
    global API_VERSION
    global API_ENV
    global X_API_KEY_VALUE
    global REGION

    global UVICORN_LOG_LEVEL
    global FASTAPI_LOG_LEVEL
    
    global AI_DB_SECRET_NAME
    global SECRET_MANAGER_ACCESS_KEY
    global SECRET_MANAGER_ID

    global USERNAMES
    global NAMES
    global PASSWORDS

    if not os.environ.get("API_ENV"): # This environtment variable is mandatory in PRD
        load_dotenv("app/config/.env",override=True)

    API_VERSION = os.environ["API_VERSION"]
    API_ENV = os.environ["API_ENV"]
    X_API_KEY_VALUE = os.environ["X_API_KEY"]
    REGION = os.environ["REGION"]

    #logging
    UVICORN_LOG_LEVEL = int(os.environ["UVICORN_LOG_LEVEL"])
    FASTAPI_LOG_LEVEL = int(os.environ["FASTAPI_LOG_LEVEL"])

    #database
    AI_DB_SECRET_NAME = os.environ['AI_DATABASE_SECRET_NAME']
    SECRET_MANAGER_ACCESS_KEY = os.environ['SECRET_MANAGER_ACCESS_KEY']
    SECRET_MANAGER_ID = os.environ['SECRET_MANAGER_ID']
    PORT = os.environ.get('PORT',27030) # if not local port comes from env, else comes from secret manager

    #authentication 
    NAMES = ast.literal_eval(os.environ['NAMES'])
    USERNAMES = ast.literal_eval(os.environ['USERNAMES'])
    PASSWORDS = ast.literal_eval(os.environ['PASSWORDS'])
