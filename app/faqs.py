from sqlite3 import Row
import streamlit as st
import db.database as mongoDB
import pandas as pd
import numpy as np
import datetime
from PIL import Image
import time
import traceback
import plotly.express as px

def FaqAnalysis():
    image = Image.open('app/logo_visor.png')
    st.image(image)

    CLIENT_NAME = st.sidebar.selectbox('Client name',mongoDB.getAllClients())
    ENV = st.sidebar.radio('Environment',["live","sandbox"])

    st.title(f'KB Analysis - {CLIENT_NAME}')
    new_title = f'<p style="font-family:Courier New; color:Grey; font-size: 25px;">{datetime.date.today().strftime("%d-%m-%Y")}</p>'
    st.markdown(new_title, unsafe_allow_html=True)    
    st.text("This is a simple dashboard that provides some usefull info about a given client FAQ's")
    
    FLOW = st.sidebar.multiselect('Flow',mongoDB.getFlows(CLIENT_NAME,ENV),["All"])
    flow_filter = mongoDB.getFlows(CLIENT_NAME,ENV)
    if 'All' in FLOW:
        flow_filter.remove('All')
    else:
        flow_filter = FLOW

    data=mongoDB.getFaqsAgg(CLIENT_NAME,flow_filter,ENV)

    categories = np.append(data.categoria.unique().astype(str),'All')
    CAT = st.sidebar.multiselect('Categoria',categories.tolist(),["All"])
    if 'All' not in CAT:
        data=data[data.categoria.isin(CAT)]
    
    faqs = np.append(data.pergunta.unique().astype(str),'All')
    FAQ = st.sidebar.multiselect('FAQ',faqs,["All"])
    if 'All' not in FAQ:
        data=data[data.pergunta.isin(FAQ)]

    st.title('FAQ Metrics')
    st.text("Some metrics about {} {} faqs".format(CLIENT_NAME,ENV))

    st.write("Number of FAQs: ",data.shape[0])
    st.write("Number of userSays: ",data.explode('userSays').shape[0])
    st.write("Number of categories: ",len(data.categoria.unique()))

    try: #Because some clients do not have date created or updated

        st.title('FAQS Created/updated by day')
        TYPE = st.selectbox('Type',['createdOn','lastUpdatedOn'])
        d= st.date_input(f'Initial date',data[TYPE].min())
        DATE = pd.Timestamp(d)
        dt = data[data[TYPE]>=DATE]
        dt[TYPE]=dt[TYPE].dt.date
        faq_by_date = pd.DataFrame(dt[TYPE].value_counts().sort_values(ascending=False).reset_index())
        st.write(faq_by_date)

    except Exception as e:
        print('ERROR IN FAQS CREATED:',e)

    try:
        st.title('Metrics Smalltalk')
        st.text("Some metrics about {} {} smalltalk".format(CLIENT_NAME,ENV))

        smalltalk = mongoDB.getSmallTalkActive(CLIENT_NAME,ENV)

        st.write("Number of active smallTalks: ",smalltalk[0]+smalltalk[1])
        st.write('Default active smallTalks:',smalltalk[0])
        st.write('Custom active smallTalks:',smalltalk[1])
        st.write('Default active smallTalk userSays:',mongoDB.getNumberOfUserSaysSmalltalkDefault(CLIENT_NAME,ENV))
        st.write('Custom active smallTalk userSays:',mongoDB.getNumberOfUserSaysSmalltalkCustom(CLIENT_NAME,ENV))
    except Exception as e:
        print('ERROR IN SMALLTALK:',e)

    try:
        st.title('Entities')
        st.text('Entities activated and used on {}'.format(CLIENT_NAME))
        entities = mongoDB.getEntities(CLIENT_NAME,ENV)
        entities = entities[entities['faqs'].str.len() != 0]
        
        entities['faqs']=entities['faqs'].map(lambda x:[y['faqId'] for y in x])

        entities = entities[['category','type','faqs']]

        st.write(entities)
    except Exception as e:
        print('ERROR IN ENTITIES:',e)

    try:
        st.title('Collocations')
        st.text('Collocations used in {}'.format(CLIENT_NAME))
        st.write('Number of collocations:',len(mongoDB.getColls(CLIENT_NAME,ENV)))
    except Exception as e:
        print('ERROR IN COLLOCATIONS:',e)

    try:
        st.title('Synonyms')
        st.text('Synonyms used in {}'.format(CLIENT_NAME))
        st.write('Number of synonyms:',len(mongoDB.getSynonyms(CLIENT_NAME,ENV)))
    except Exception as e:
        print('ERROR IN SYNONYMS:',e)
    
    try:
        st.title('Lists')
        st.text('Lists used in {}'.format(CLIENT_NAME))
        query = {
            'faq':True,
            'flows':FLOW 
        } if 'All' not in FLOW else {
            'faq':True
        }
        lists = pd.DataFrame(mongoDB.getLists(
            CLIENT_NAME,
            ENV,
            query,
            {'_id':0,'name':1,'words':1,'flows':1}
        ))
        st.write(lists)
    except Exception as e:
        print('ERROR IN LISTS:',e)

    try:
        st.title('Context')
        st.text('Context activated for {}'.format(CLIENT_NAME))
        context = pd.DataFrame(mongoDB.getContext(CLIENT_NAME,ENV,{},{'_id':0,'name':1,'lifespan':1}))
        st.write(context)
    except Exception as e:
        print('ERROR IN LISTS:',e)

    try:
        st.title('Assistants')
        st.text("All the assistants available in {} {} faqs".format(CLIENT_NAME,ENV))

        assistants = [] 
        for record in data['assistants']:
            if record[0]['assistant'] not in assistants:
                assistants.append(record[0]['assistant'])

        st.write("Number of assistants: ",len(assistants))
    except Exception as e:
        print('ERROR IN ASSISTANTS:',e)

    try:
        st.title("Top 10 words")
        st.text("Top 10 words in {} {} faqs".format(CLIENT_NAME,ENV))
        words=np.array([x['text'].split(' ') for x in data.explode('userSays')['userSays'].tolist()])

        topWords = pd.Series(np.hstack(words)).value_counts()[:10]
        for idx,index in enumerate(topWords.to_dict()):
            st.write(str(idx+1)+")",index,'->',topWords.to_dict()[index])
    except Exception as e:
        print('ERROR IN TOP:',e)

    ################################################################
    try:
        @st.cache  # IMPORTANT: Cache the conversion to prevent computation on every rerun
        def convert_df(df):
            """Generates a link allowing the data in a given panda dataframe to be downloaded
            in:  dataframe
            out: href string
            """
            return df.to_csv(index=False).encode('utf-8')
            #return df.to_csv(index=False).encode('utf-8')
            
        st.title('Download data')
        st.text("Get all the data used for this dashboard".format(CLIENT_NAME,ENV))

        st.download_button(
            label="Download data",
            data=convert_df(data),
            file_name=f'faqs_{CLIENT_NAME}_{ENV}.csv',
            mime='text/csv',
        )

    except Exception as e:
        print(e,traceback.format_exc())
        st.error('Error when trying to download data....')