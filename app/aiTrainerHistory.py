import streamlit as st
import db.database as mongoDB
import pandas as pd
import numpy as np
from PIL import Image
import datetime
from dateutil.relativedelta import relativedelta
import plotly.express as px
import traceback
import base64

def AiTrainerHistory():
    image = Image.open('app/logo_visor.png')
    st.image(image)
    date1monthAgo = datetime.date.today() - relativedelta(months=1)

    CLIENT_NAME = st.sidebar.selectbox('Client name',mongoDB.getAllClients())
    BEGIN = st.sidebar.date_input("Begin",date1monthAgo)
    END = st.sidebar.date_input("End")

    data = mongoDB.getAiTrainerHistory(CLIENT_NAME,BEGIN,END)

    assistants = np.append(data.assistant.unique().astype(str),'All')
    ASSISTANT = st.sidebar.multiselect('Assistants',assistants.tolist(),["All"])
    if 'All' not in ASSISTANT:
        data=data[data.assistant.isin(ASSISTANT)]

    st.title(f'Ai Trainer History - {CLIENT_NAME}')
    new_title = f'<p style="font-family:Courier New; color:Grey; font-size: 25px;">{datetime.date.today().strftime("%d-%m-%Y")}</p>'
    st.markdown(new_title, unsafe_allow_html=True) 
    st.text("This is a simple dashboard that provides some usefull info about AiTrainer history. The environment is always sandbox")
    
    try:
        data['date2'] = data['date'].apply(lambda x:x.date())
        data['count'] = 1  

        faqs = mongoDB.getFaqs(CLIENT_NAME,'sandbox',project={'_id':1,'pergunta':1}) #sandbox because aiTrainer is always sandbox
        faqs._id=faqs._id.astype('str')
        faqs = dict(zip(faqs._id, faqs.pergunta))

        st.title('Accepted questions by FAQ')
        st.text("Plot of accepted question per FAQ")
        st.text(f'Beginning date: {data.date.min().date()}')

        accepted = data[data.status=='accepted']
        accepted['answer_faq'] = accepted['answer_faq'].astype('str')

        def transform(x): 
            try: 
                return faqs[x]
            except:
                return "Accept FAQ does no longer exists in FAQ's"
        
        accepted = pd.DataFrame(accepted.answer_faq.value_counts().sort_values(ascending=True).reset_index())
        accepted['Faq'] = accepted['index'].apply(lambda x:transform(x))
        fig = px.bar(accepted, x='answer_faq', y='index', orientation='h', color='answer_faq',
                    color_continuous_scale=px.colors.sequential.Agsunset,
                    labels={'index':'Faq id', 'answer_faq':'Count'},
                    hover_data=["index","Faq","answer_faq"],
                    opacity=0.8,
                    text_auto=True
                    )
        fig.update_layout(bargap=0.4,autosize=True)
        st.plotly_chart(fig)

        st.title('Rejected questions by FAQ')
        st.text("Plot of rejected question per FAQ")
        st.text(f'Beginning date: {data.date.min().date()}')

        rejected = data[data.status=='rejected']
        rejected['answer_faq'] = rejected['answer_faq'].astype('str')

        def transform(x): 
            try: 
                return faqs[x]
            except:
                return "Accept FAQ does no longer exists in FAQ's"
        
        rejected = pd.DataFrame(rejected.answer_faq.value_counts().sort_values(ascending=True).reset_index())
        rejected['Faq'] = rejected['index'].apply(lambda x:transform(x))
        fig = px.bar(rejected, x='answer_faq', y='index', orientation='h', color='answer_faq',
                    color_continuous_scale=px.colors.sequential.Agsunset,
                    labels={'index':'Faq id', 'answer_faq':'Count'},
                    hover_data=["index","Faq","answer_faq"],
                    opacity=0.8,
                    text_auto=True
                    )
        fig.update_layout(bargap=0.4,autosize=True)
        st.plotly_chart(fig)


    except Exception as e:
        print(e,traceback.format_exc())
        st.error('Error showing Accepted Faqs plot! Probably there is not available data for the chosen time span!')

    try:
        @st.cache  # IMPORTANT: Cache the conversion to prevent computation on every rerun
        def convert_df(df):
            """Generates a link allowing the data in a given panda dataframe to be downloaded
            in:  dataframe
            out: href string
            """
            return df.to_csv(index=False).encode('utf-8')
            
        st.title('Download data')
        st.text("Get all the data used for this dashboard".format(CLIENT_NAME,BEGIN,END))

        st.download_button(
            label="Download data",
            data=convert_df(data),
            file_name=f'aiTrainer_{CLIENT_NAME}.csv',
            mime='text/csv',
        )

    except Exception as e:
        print(e,traceback.format_exc())
        st.error('Error when trying to download data....')


