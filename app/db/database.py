from unicodedata import name
import boto3
import pandas as pd
import config.variables as variables
from pymongo import MongoClient
from botocore.exceptions import ClientError
import json
from typing import List
from datetime import datetime, timedelta, time
from typing import Union
from dateutil import parser
import string

class MongoDb:
    def __init__(self, file="application.db"):
        self.api_env = variables.API_ENV
        self.region = variables.REGION
        self.ai_db_secret_name = variables.AI_DB_SECRET_NAME
        self.secret_manager_access_key = variables.SECRET_MANAGER_ACCESS_KEY
        self.secret_manager_id = variables.SECRET_MANAGER_ID
        # _,_,self.host,self.port = "","",'127.0.0.1',variables.PORT if self.api_env == 'local' else self.get_secret(self.region,
        #                     self.secret_manager_id,self.secret_manager_access_key,self.ai_db_secret_name)
        self.username,self.password,self.host,self.port = self.get_secret(self.region,self.secret_manager_id,
                           self.secret_manager_access_key,self.ai_db_secret_name)
        self.host,self.port = ('127.0.0.1',variables.PORT) if self.api_env == 'local' else (self.host,self.port)
        

    def __enter__(self): # Magic function with this we do with open('application.db) and automatically runs enter code
        self.conn = self.init_db(self.username,self.password,self.api_env,self.port,self.host)
        return self.conn

    def __exit__(self, type, value, traceback):
        self.conn.close()

    def init_db(self,username,password,api_env,port,host):
        pem_path=f'rds-combined-ca-bundle.pem'
        dbUri = f'mongodb://{username}:{password}@{host}:{port}/?tls=true&tlsCAFile={pem_path}&tlsInsecure=true&directConnection=true&retryWrites=false' 
        client = MongoClient(dbUri)
        return client

    def get_secret(self,region,secret_manager_id,secret_manager_access_key,ai_db_secret_name):
        session = boto3.session.Session()
        client = session.client(
            service_name='secretsmanager',
            region_name=region,
            aws_access_key_id=secret_manager_id, 
            aws_secret_access_key=secret_manager_access_key
        )
        try:
            get_secret_value_response = json.loads(client.get_secret_value(SecretId=ai_db_secret_name)['SecretString'])
            username = get_secret_value_response['username']
            password = get_secret_value_response['password']
            host = get_secret_value_response['host']
            port = get_secret_value_response['port']
            ec2host = get_secret_value_response['ec2Host']
            privateKey = get_secret_value_response['privatekey']
            return username,password,host,port

        except ClientError as e:
            if e.response['Error']['Code'] == 'ResourceNotFoundException':
                print("The requested secret " + ai_db_secret_name + " was not found")
            elif e.response['Error']['Code'] == 'InvalidRequestException':
                print("The request was invalid due to:", e)
            elif e.response['Error']['Code'] == 'InvalidParameterException':
                print("The request had invalid params:", e)
            elif e.response['Error']['Code'] == 'DecryptionFailure':
                print("The requested secret can't be decrypted using the provided KMS key:", e)
            elif e.response['Error']['Code'] == 'InternalServiceError':
                print("An error occurred on service side:", e)
            return

def getAllClients() -> List[str]:
    with MongoDb("application.db") as db:
        dbNames=db.list_database_names()
        for name in ['general','greetings','greetingsen','greetingses']:
            try:
                dbNames.remove(name)
            except Exception as e:
                continue # next element 
        return dbNames

def getClientLanguage(client:str) -> str:
    with MongoDb("application.db") as db:
        return list(db['general']['specifications'].find({'clientName':client}))[0]

def getFaqs(client:str,env:str,query:dict={},project:dict={}) -> pd.DataFrame:
    with MongoDb("application.db") as db:
        name = 'faq-sandbox' if (env=='sandbox') else 'faq'
        db = db[client]
        if not project: # project everything
            collection = list(db[name].find(query))
        else:
            collection = list(db[name].find(query,project))
        faqs = pd.DataFrame(collection)
        return faqs

def getFaqsAgg(client: str, flow: str, env:str) -> pd.DataFrame:
    with MongoDb("application.db") as db:
        name = 'faq-sandbox' if (env=='sandbox') else 'faq'
        query = [
            {"$match": {"resposta.flowId": {"$in": flow}}},
        ]
        faqs=pd.DataFrame(list(db[client][name].aggregate(query)))
        return faqs

def getSynonyms(client: str, env:str) -> list:
    with MongoDb("application.db") as db:
        name = 'synonyms-sandbox' if (env=='sandbox') else 'synonyms'
        return list(db[client][name].find({}))

def getColls(client: str, env:str) -> list:
    with MongoDb("application.db") as db:
        name = 'collocations-sandbox' if (env=='sandbox') else 'collocations'
        return list(db[client][name].find({}))

def getLists(client: str,env:str,query:dict={},project:dict={}) -> list:
    with MongoDb("application.db") as db:
        name = 'lists-sandbox' if (env=='sandbox') else 'lists'
        return list(db[client][name].find(query,project))

def getContext(client: str,env:str,query:dict={},project:dict={}) -> list:
    with MongoDb("application.db") as db:
        name = 'context-sandbox' if (env=='sandbox') else 'context'
        return list(db[client][name].find(query,project))

def getSmallTalk(client:str,env:str) -> pd.DataFrame:
    with MongoDb("application.db") as db:
        name = 'smallTalk-sandbox' if (env=='sandbox') else 'smallTalk'
        db = db[client]
        collection = list(db[name].find())
        smallTalks = pd.DataFrame(collection)
        return smallTalks

def getSmallTalkActiveDefault(client:str,env:str) -> pd.DataFrame:
    with MongoDb("application.db") as db:
        name = 'smallTalk-sandbox' if (env=='sandbox') else 'smallTalk'
        db = db[client]
        collection = list(db[name].find({'active':True,'smalltalkType':'default'}))
        smallTalks = pd.DataFrame(collection)
        return smallTalks

def getSmallTalkActive(client:str,env:str) -> List[int]:
    with MongoDb("application.db") as db:
        name = 'smallTalk-sandbox' if (env=='sandbox') else 'smallTalk'
        db = db[client]
        custom = len(list(db[name].find({'active':True,'smalltalkType':'custom'})))
        default = len(list(db[name].find({'active':True,'smalltalkType':'default'})))
        return [default,custom]

def getFlows(client:str,env:str) -> list:
    with MongoDb("application.db") as db:
        flowIds = ['All']
        name = 'faq-sandbox' if (env=='sandbox') else 'faq'
        for faq in list(db[client][name].find({})):
            for resposta in faq['resposta']:
                if resposta['flowId'] not in flowIds:
                    flowIds.append(resposta['flowId'])
        return flowIds

def getNumberOfUserSaysSmalltalkCustom(client:str,env:str) -> int:
    with MongoDb("application.db") as db:
        try:
            name = 'smallTalk-sandbox' if (env=='sandbox') else 'smallTalk'
            db = db[client]        
            custom = list(db[name].aggregate([
            {'$match': { 'active':True,'smalltalkType':'custom'}},
            {'$unwind':'$userSays'},
            {'$group':{'_id':'$_id','sum':{'$sum': 1}}},
            {'$group': {'_id': 'null', 'total_sum': {'$sum': '$sum'}}}
            ]))
            return custom[0]['total_sum']
        except:
            return 0

def getNumberOfUserSaysSmalltalkDefault(client:str,env:str) -> int:
    with MongoDb("application.db") as db:
        user_says_count = 0
        try:
            db = db['general']        
            default_active = getSmallTalkActiveDefault(client,env)
            default = pd.DataFrame(list(db['smallTalk'].find({})))
            for sm_id in default_active.loc[:,'_id'].values:
                user_says_count+=len(default[default['_id']==sm_id]['userSays'].values[0])
            return user_says_count
        except:
            return user_says_count

def getAnswerHistory(client:str,env:str,begin:datetime.date,end:datetime.date) -> pd.DataFrame:
    with MongoDb("application.db") as db:
        begin =  datetime.combine(begin, time.min)
        end =  datetime.combine(end, time.min)
        return pd.DataFrame(list(db[client]['answerHistory'].find({
            'environment':env,
            'date':{'$gte':begin,'$lte':end}
        })))

def getAiTrainerHistory(client:str,begin:datetime.date,end:datetime.date) -> pd.DataFrame:
    with MongoDb("application.db") as db:
        begin =  datetime.combine(begin, time.min)
        end =  datetime.combine(end, time.min)
        return pd.DataFrame(list(db[client]['aiTrainerHistory'].find({
            'date':{'$gte':begin,'$lte':end}
        })))

def getProbabilities(client:str):
    with MongoDb("application.db") as db:
        try:
            probs = list(db['general']['specifications'].find({
                'clientName':client
            },{
                '_id':0,
                'probabilities':1
            }))[0]
        except:
            probs = list(db['general']['specifications'].find({},{
                '_id':0,
                'probabilities':1
            }))[0]
        return probs

def getEntities(client:str,env:str):
    with MongoDb("application.db") as db:
        name = 'entities-sandbox' if (env=='sandbox') else 'entities'
        return pd.DataFrame(list(db[client][name].find({})))

############################################## WORDCLOUD ##############################################

def generatePreprocessedWordsForWordcloud(
        client: str,
        end: Union[str, datetime] = datetime.today(),
        language: str = "pt",
        type: str = None,
        env: str = "live"
    ):
    """
    Filters questions by date range (7 and 30 days) and preprocesses them
    by removing punctuation and stopwords in a mongo-centric query.

    Results are saved in the wordcloud_prep_words collection,
    to be used for unigram and bigram generation.
    If wordcloud_prep_words does not exist, it is automatically created.
    If it already exists, are contents are replaced by the results of
    this query.

    Parameters
    ----------
    end : Union[str, datetime]
        end date of the date range
        7 and 30 days prior intervals are calculated based on this date
    language: str, optional
        The language of the client to consider for stopword removal,
        by default "pt"
    type : str, optional
        type of question to consider, by default None.
        if None, all question types are considered
    env : str, optional
        the db environment, by default "live"
    """

    # parse end date
    with MongoDb("application.db") as db:

        if isinstance(end, str):
            end = parser.parse(end)

        # create list of punctuation marks to remove
        punctuation_list = list(string.punctuation)
        punctuation_list.remove("$")  # removed due to parsing problems with pymongo
        punctuation_list.remove("-")  # keep - for hyphenated words, just in case

        # get stopword list from database for the client's language
        stopword_list = list(db['general']['stopwords-wordcloud'].find({"language":language}))
        stopword_list = [sw['word'] for sw in stopword_list]
        stopword_list.append("")  # processing generates empty strings, remove them

        # if a question type is specified, add this condition to the match step
        if type:
            match_step = {
                "$match": {
                    "type": type,
                    "environment": env,
                    "date": {"$gte": end - timedelta(days=30), "$lt": end},
                }
            }
        # otherwise, ignore it and only match by date interval and environmet
        else:
            match_step = {
                "$match": {
                    "environment": env,
                    "date": {"$gte": end - timedelta(days=30), "$lt": end},
                }
            }

        # keep the fields that are relevant to store for the result
        fields_of_interest_step = {
            "$project": {
                "question": 1,
                "type": 1,
                "environment": 1,
                # create period field for 7 or 30 day answers
                "period": {
                    "$cond": {
                        "if": {"$gte": ["$date", end - timedelta(days=7)]},
                        "then": 7,
                        "else": 30,
                    }
                },
            }
        }

        # remove punctuation
        # each word in the question is decomposed into its chars
        # chars that are punctuation are filtered out
        # and words are regenerated withou punctuation in the end
        remove_punctuation_step = {
            "$project": {
                "type": 1,
                "environment": 1,
                "period": 1,
                "words": {
                    "$map": {
                        # splits all questions by spaces
                        "input": {"$split": [{"$toLower": "$question"}, " "]},
                        "as": "item",
                        "in": {
                            "$reduce": {
                                "input": {
                                    "$filter": {
                                        "input": {
                                            "$map": {
                                                "input": {
                                                    # gets the index of each char
                                                    "$range": [
                                                        0,
                                                        {"$strLenCP": "$$item"},
                                                    ]
                                                },
                                                "as": "charIdx",
                                                "in": {
                                                    "$substrCP": [
                                                        "$$item",
                                                        "$$charIdx",
                                                        1,
                                                    ]
                                                },
                                            }
                                        },
                                        "as": "char",
                                        # only keeps chars that are not punctuation
                                        "cond": {
                                            "$not": {
                                                "$in": ["$$char", punctuation_list]
                                            }
                                        },
                                    }
                                },
                                # regenerates words by concatenation
                                "initialValue": "",
                                "in": {"$concat": ["$$value", "$$this"]},
                            }
                        },
                    }
                },
            }
        }

        # remove stopwords
        # filters out words present in the stopword list
        remove_stopwords_step = {
            "$project": {
                "type": 1,
                "environment": 1,
                "period": 1,
                "words": {
                    "$filter": {
                        "input": "$words",
                        "as": "word",
                        # filter out words that are in the stopword list
                        "cond": {"$not": {"$in": ["$$word", stopword_list]}},
                    }
                },
            }
        }

        # out step of the pipeline
        # results are stored in a temporary collection,
        # to be used for unigram and bigram calculations
        out_step = {"$out":"wordcloud_prep_words"}

        # build the pipeline as a list
        pipeline = [
            match_step,
            fields_of_interest_step,
            remove_punctuation_step,
            remove_stopwords_step,
            out_step,
        ]

        # perform query
        # empty return, query results in creation of a temp collection
        return list(db[client].aggregate(aggregate="answerHistory",pipeline=pipeline))

def getWordcloudPrepWords(client:str):
    with MongoDb("application.db") as db:
        return pd.DataFrame(list(db[client]['wordcloud_prep_words'].find({})))

def get7DayUnigrams(client:str):
    with MongoDb("application.db") as db:
        """
        Creates the 7-day unigram list, with top-30 unigrams, for the
        preprocessed answers available in this collection.
        Unigrams are ordered by weight (number of times unigram
        appeared in answers)

        Returns
        -------
        list
            list of top-30 7-day unigrams
        """
        pipeline = [
            # only use answers in the last 7 days
            {"$match": {"period": 7}},
            # unwind and group by word,
            # to accumulate number of times each word is used
            {"$unwind": "$words"},
            {"$group": {"_id": "$words", "weight": {"$sum": 1}}},
            # sort and limit by top-30 words
            {"$sort": {"weight": -1, "_id": 1}},
            {"$limit": 30},
            # project to present relevant fields in the result
            {
                "$project": {
                    "_id": 0,
                    "text": "$_id",
                    "type": "unigram",
                    "date": datetime.now(),
                    "rank": None,
                    "weight": 1,
                    "userSayList": None,
                }
            },
        ]
        return list(db[client].aggregate(aggregate="wordcloud_prep_words",pipeline=pipeline))

def get30DayUnigrams(client:str):
    with MongoDb("application.db") as db:
        """
        Creates the 30-day unigram list, with top-30 unigrams, for the
        preprocessed answers available in this collection.
        Unigrams are ordered by weight (number of times unigram
        appeared in answers)

        Returns
        -------
        list
            list of top-30 30-day unigrams
        """

        pipeline = [
            # don't filter out period=30 questions
            {"$unwind": "$words"},
            {"$group": {"_id": "$words", "weight": {"$sum": 1}}},
            {"$sort": {"weight": -1, "_id": 1}},
            {"$limit": 30},
            {
                "$project": {
                    "_id": 0,
                    "text": "$_id",
                    "type": "unigram",
                    "date": datetime.now(),
                    "rank": None,
                    "weight": 1,
                    "userSayList": None,
                }
            },
        ]
        return list(db[client].aggregate(aggregate="wordcloud_prep_words",pipeline=pipeline))

def get7DayBigrams(client:str):
    with MongoDb("application.db") as db:
        """
        Creates the 7-day bigram list, with top-30 bigrams, for the
        preprocessed answers available in this collectiln.
        Bigrams are ordered by weight (number of time bigram
        appeared in answers)

        Returns
        -------
        list
            list of top-30 7-day bigrams
        """
        pipeline = [
            # only use answers in the last 7 days
            {"$match": {"period": 7}},
            {
                # create bigrams by concatenating adjacent words
                "$project": {
                    "words": {
                        "$map": {
                            "input": {
                                "$range": [0, {"$add": [{"$size": "$words"}, -1]}]
                            },
                            "as": "idx",
                            "in": {
                                "$concat": [
                                    {"$arrayElemAt": ["$words", "$$idx"]},
                                    " ",
                                    {
                                        "$arrayElemAt": [
                                            "$words",
                                            {"$add": ["$$idx", 1]},
                                        ]
                                    },
                                ]
                            },
                        }
                    }
                }
            },
            # unwind and group by bigram,
            # to accumulate number of times each bigram is used
            {"$unwind": "$words"},
            {"$group": {"_id": "$words", "weight": {"$sum": 1}}},
            # sort and limit by top-30 words
            {"$sort": {"weight": -1, "_id": 1}},
            {"$limit": 30},
            # project to present relevant fields in the result
            {
                "$project": {
                    "_id": 0,
                    "text": "$_id",
                    "type": "bigram",
                    "date": datetime.now(),
                    "rank": None,
                    "weight": 1,
                    "userSayList": None,
                }
            },
        ]
        return list(db[client].aggregate(aggregate="wordcloud_prep_words",pipeline=pipeline))

def get30DayBigrams(client:str):
    with MongoDb("application.db") as db:
        """
        Creates the 30-day bigram list, with top-30 bigrams, for the
        preprocessed answers available in this collectiln.
        Bigrams are ordered by weight (number of time bigram
        appeared in answers)

        Returns
        -------
        list
            list of top-30 30-day bigrams
        """

        pipeline = [
        # don't filter out period=30 questions
        {
            "$project": {
                "words": {
                    "$map": {
                        "input": {
                            "$range": [0, {"$add": [{"$size": "$words"}, -1]}]
                        },
                        "as": "idx",
                        "in": {
                            "$concat": [
                                {"$arrayElemAt": ["$words", "$$idx"]},
                                " ",
                                {
                                    "$arrayElemAt": [
                                        "$words",
                                        {"$add": ["$$idx", 1]},
                                    ]
                                },
                            ]
                        },
                    }
                }
            }
        },
        {"$unwind": "$words"},
        {"$group": {"_id": "$words", "weight": {"$sum": 1}}},
        {"$sort": {"weight": -1, "_id": 1}},
        {"$limit": 30},
        {
            "$project": {
                "_id": 0,
                "text": "$_id",
                "type": "bigram",
                "date": datetime.now(),
                "rank": None,
                "weight": 1,
                "userSayList": None,
            }
        }]
        return list(db[client].aggregate(aggregate="wordcloud_prep_words",pipeline=pipeline))

def updateWordcloud7(client:str,wordcloud:list):
    with MongoDb("application.db") as db:
        db[client]['wordcloud7'].delete_many({})
        return db[client]['wordcloud7'].insert_many(wordcloud)

def updateWordcloud30(client:str,wordcloud:list):
    with MongoDb("application.db") as db:
        db[client]['wordcloud30'].delete_many({})
        return db[client]['wordcloud30'].insert_many(wordcloud)

def getWordcloud7(client:str,query:dict={},project:dict={}):
    with MongoDb("application.db") as db:
        if not project: # project everything
            collection = list(db[client]['wordcloud7'].find(query))
        else:
            collection = list(db[client]['wordcloud7'].find(query,project))
        return collection

def getWordcloud30(client:str,query:dict={},project:dict={}):
    with MongoDb("application.db") as db:
        if not project: # project everything
            collection = list(db[client]['wordcloud30'].find(query))
        else:
            collection = list(db[client]['wordcloud30'].find(query,project))
        return collection