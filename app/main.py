import streamlit as st
import db.database as mongoDB
import streamlit_authenticator as stauth
import config.variables as variables
from dotenv import load_dotenv
from config.variables import load
import pandas as pd
import numpy as np
from PIL import Image
import base64
from datetime import date
from faqs import FaqAnalysis 
from history import AnswerHistory,updateWordclouds
from aiTrainerHistory import AiTrainerHistory
from apscheduler.schedulers.background import BackgroundScheduler

sched = BackgroundScheduler()
sched.start()

def main():
    """
    Dashboard selection after login
    """
    pages = {'KB Analysis':FaqAnalysis,'Answer History':AnswerHistory,'AiTrainerHistory':AiTrainerHistory}
    choice = st.sidebar.radio("Page ",tuple(pages.keys()))
    pages[choice]()

def authenticator():
    """
    Authentication for dashboard access
    """
    image = Image.open('app/logo_visor.png')
    st.sidebar.image(image,width=200)
    st.sidebar.title(f'AI Dashboard')
    st.sidebar.text(f'Dashboard with AI metrics!')

    hashed_passwords = stauth.Hasher(variables.PASSWORDS).generate()
    authenticator = stauth.Authenticate(variables.NAMES,variables.USERNAMES, hashed_passwords,'some_cookie_name','some_signature_key', cookie_expiry_days=0.5)
    name, authentication_status, username = authenticator.login('Login', 'sidebar')
    if authentication_status:
        authenticator.logout('Logout', 'main')
        st.write('Welcome *%s*' % (name))
    elif authentication_status == False:
        st.error('Username/password is incorrect')
    elif authentication_status == None:
        st.warning('Please enter your username and password')
    return authentication_status

@sched.scheduled_job("cron", id="daiy_wordcloud_generation", hour=4)
def run_wordcloud():
    try:
        clients = mongoDB.getAllClients()
        for client in clients:
            updateWordclouds(client)
    except Exception as e:
        print('Error when creating wordclouds')

if __name__ == '__main__':
    st.set_page_config(page_title='AI Metrics', page_icon="😎")
    load() # Load env variables
    if authenticator(): # Authenticate client
        main()
