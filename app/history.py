from wordcloud import WordCloud
import streamlit as st
import db.database as mongoDB
import streamlit_authenticator as stauth
import config.variables as variables
from dotenv import load_dotenv
from config.variables import load
import pandas as pd
import numpy as np
from PIL import Image
import base64
from datetime import datetime
import traceback
from dateutil.relativedelta import relativedelta
import plotly.express as px
import matplotlib.pyplot as plt

st.set_option('deprecation.showPyplotGlobalUse', False)

def AnswerHistory():
    image = Image.open('app/logo_visor.png')
    st.image(image)
    date1monthAgo = datetime.today() - relativedelta(months=1)

    CLIENT_NAME = st.sidebar.selectbox('Client name',mongoDB.getAllClients())
    ENV = st.sidebar.radio('Environment',["live","sandbox"])
    BEGIN = st.sidebar.date_input("Begin",date1monthAgo)
    END = st.sidebar.date_input("End")

    data = mongoDB.getAnswerHistory(CLIENT_NAME,ENV,BEGIN,END)
    
    FLOW = st.sidebar.multiselect('Flow',mongoDB.getFlows(CLIENT_NAME,ENV),["All"])
    flow_filter = mongoDB.getFlows(CLIENT_NAME,ENV)
    if 'All' in FLOW:
        flow_filter.remove('All')
    else:
        flow_filter = FLOW

    data=data[data.project.isin(flow_filter)]

    categories = np.append(data.category.unique().astype(str),'All')
    CAT = st.sidebar.multiselect('Categoria',categories.tolist(),["All"])
    if 'All' not in CAT:
        data=data[data.category.isin(CAT)]
    
    faqs = np.append(data.questionTitles.unique().astype(str),'All')
    FAQ = st.sidebar.multiselect('FAQ',faqs,["All"])
    if 'All' not in FAQ:
        data=data[data.questionTitles.isin(FAQ)]

    st.title(f'Answer History - {CLIENT_NAME}')
    new_title = f'<p style="font-family:Courier New; color:Grey; font-size: 25px;">{datetime.today().strftime("%d-%m-%Y")}</p>'
    st.markdown(new_title, unsafe_allow_html=True)    
    st.text("This is a simple dashboard that provides some usefull info about answer history")
    
    PROBS = mongoDB.getProbabilities(CLIENT_NAME)['probabilities']    
    probQuestion = PROBS['probabilityQuestion']*100
    probUnsure = PROBS['unsureProbabilityQuestion']*100 
    probSmallTalk = PROBS['greetingsQuestion']*100

    st.write('Correct answer: above than',probQuestion,'%')
    st.write('Unsure answer: between',probUnsure,'%','and',probQuestion,'%')
    st.write('SmallTalk answer: equal or above than',probSmallTalk,'%')

    try:
        data['date2'] = data['date'].apply(lambda x:x.date())
        data['count'] = 1

        # New types for answe_by_bd
        data.loc[(data.type=='correct_answer') & (data.answer_probability.isnull()), 'type'] = 'answer_by_bd'  
        data.loc[(data.type=='greetings_answer') & (data.answer_probability.isnull()), 'type'] = 'answer_by_bd_smalltalk' 

        st.title('Answer by day')
        st.text("Plot of total number answers by day")

        fig = px.histogram(data, x="date2", y='count',color='type', title="Answer type by day",
            color_discrete_sequence=px.colors.qualitative.Prism_r,
            labels={'count':'Count','date2':'Date','type':'Type'},
            opacity=0.8,
            nbins=data['date2'].nunique(),
            width=850,height=450)
        fig.update_layout(bargap=0.5,autosize=True)
        fig.update_xaxes(tickangle=325,showgrid=True)
        st.plotly_chart(fig)
    
    except Exception as e:
        st.error('Error showing Answer by Day plot! Probably there is not available data for the chosen time span!')

    st.title(f'Conversion rates')
    st.text("Conversion rates by answer type")
    try:
        type_rate = pd.DataFrame(data.type.value_counts().apply(lambda x: round((x/data.shape[0])*100,2)).sort_values(ascending=True))
        type_rate = type_rate.reset_index()
        fig = px.bar(type_rate, x='type', y='index', orientation='h',color='type',
                    labels={'index':'Type', 'type':'Conversion rate (%)'},
                    color_continuous_scale = px.colors.sequential.Agsunset,
                    text_auto=True,width=770,height=450,opacity=0.8
                    )
        fig.update_layout(bargap=0.4,autosize=True)
        fig.update_xaxes(showgrid=True)
        st.plotly_chart(fig)

    except Exception as e:
         st.error('Error showing Conversion rates plot!!')

    st.title('Individual metrics on detail')
    st.text('Select the granularity of analysis (category or faq) and investigate how conversion rates are for that subset')
    try:
        SELECTION = st.selectbox('Select granularity',['category','faq'])
        SELECTION = 'questionTitles' if SELECTION == 'faq' else SELECTION
        SELECTED=st.selectbox(SELECTION,data[SELECTION].dropna().unique())

        f = data[data[str(SELECTION)]==str(SELECTED)]
        f_len = f.shape[0]
        f = pd.DataFrame(f.type.value_counts())
        f['type_per']=f['type'].apply(lambda x: round((x/f_len)*100,2))
        f = f.sort_values(by='type_per',ascending=True).reset_index()
        fig = px.bar(f, x='type_per', y='index', orientation='h',color='type_per',
                    labels={'index':'Type','type_per':'Conversion rate (%)','type':'Count'},
                    color_continuous_scale = px.colors.sequential.Agsunset,
                    text_auto=True,width=770,height=450,opacity=0.8
                    )
        fig.update_layout(bargap=0.4,autosize=True)
        fig.update_xaxes(showgrid=True)
        st.plotly_chart(fig)

    except Exception as e:
        print(e)
        st.error('Error showing conversion rates plot!!')

    try:
        st.title(f'Wordcloud')
        st.text('Create wordclouds for the given client for a defined period and type')
        PERIOD = st.selectbox('Select period',[7,30])
        TYPE = st.selectbox('Select type',['unigram','bigram'])

        if PERIOD==7:
            wc = mongoDB.getWordcloud7(client=CLIENT_NAME,query={'type':TYPE})
        elif PERIOD==30:
            wc = mongoDB.getWordcloud30(client=CLIENT_NAME,query={'type':TYPE})
        
        wc_text = ','.join([obj['text'].replace(" ","_") for obj in wc])
        wordcloud = WordCloud().generate(wc_text)
        plt.imshow(wordcloud, interpolation='bilinear')
        plt.axis("off")  
        st.pyplot()

        st.button(label='Generate new wordclouds',
                  key='Wordcloud',
                  help='Click here when you want to generate new wordclouds for the given client. The showcased wordcloud are done with the last calculated data.',
                  on_click=updateWordclouds,
                  kwargs={'clientName':CLIENT_NAME})

    except Exception as e:
        print("Error creating wordcloud",e)

    try:
        @st.cache  # IMPORTANT: Cache the conversion to prevent computation on every rerun
        def convert_df(df):
            """Generates a link allowing the data in a given panda dataframe to be downloaded
            in:  dataframe
            out: href string
            """
            return df.to_csv(index=False).encode('utf-8')
            
        st.title('Download data')
        st.text("Get all the data used for this dashboard".format(CLIENT_NAME,ENV,BEGIN,END))

        st.download_button(
            label="Download data",
            data=convert_df(data),
            file_name=f'history_{CLIENT_NAME}_{FLOW}_{ENV}.csv',
            mime='text/csv',
        )

    except Exception as e:
        print(e,traceback.format_exc())
        st.error('Error when trying to download data....')

def updateWordclouds(clientName:str):
    try:
        print(f'Updating {clientName} wordclouds...')
        language = mongoDB.getClientLanguage(client=clientName)
        language = language['language']['code']
        mongoDB.generatePreprocessedWordsForWordcloud(
            client=clientName,
            end=datetime.today(),
            language=language
        )
        wc7 = [*mongoDB.get7DayUnigrams(clientName),*mongoDB.get7DayBigrams(clientName)] 
        wc30 = [*mongoDB.get30DayUnigrams(clientName),*mongoDB.get30DayBigrams(clientName)]
            
        mongoDB.updateWordcloud7(clientName,wc7)
        mongoDB.updateWordcloud30(clientName,wc30)
        print(f'Done! {clientName} wordclouds updated')
        return
    except Exception as e:
        print('Error when creating wordcloud for',clientName)
        print(e,traceback.format_exc())